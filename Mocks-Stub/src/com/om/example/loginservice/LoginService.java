package com.om.example.loginservice;

public class LoginService {
	
	private final IAccountRepository accountRepository;
	private int failedAttempts = 0;
	private String previousAccountId = "";
	private boolean changeOK = false;
	String candidate = "";

	public LoginService(IAccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
 	
	public void login(String accountId, String password) {
		IAccount account = accountRepository.find(accountId);
		String candidate = "";
		
		if (account == null)
	         throw new AccountNotFoundException();
        if (account.passwordMatches(password)) {
    		if (account.isLoggedIn())
    			throw new AccountLoginLimitReachedException();
    		if (account.isRevoked())
    			throw new AccountRevokedException();
    		if (account.isPasswordExpired()) {
    			if (account.isPasswordChanged()) {
    				if (account.isPasswordAmongLast24()) {
    					throw new ReusedPasswordException();
    				}
    			}
				else {
    				throw new ExpiredPasswordException();
				}
    		}
    		if (account.isPasswordTemporary()) {
    			if (account.isPasswordChanged()) {
    				if (account.isPasswordAmongLast24()) {
    					throw new ReusedPasswordException();
    				}
    			}
				else {
    				throw new TemporaryPasswordException();
				}
    		}
    		account.setLoggedIn(true);
         } else {
            if (previousAccountId.equals(accountId))
               ++failedAttempts;
            else {
               failedAttempts = 1;
               previousAccountId = accountId;
            }
         }
      
        if (failedAttempts == 3)
           account.setRevoked(true);
   }
}
