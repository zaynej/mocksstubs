
package com.om.example.loginservice;
 
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;

import org.junit.Before;
import org.junit.Test;

import com.om.example.loginservice.*;
 
public class LoginServiceTest {
   private IAccount account;
   private IAccountRepository accountRepository;
   private LoginService service;
 
   @Before
   public void init() {
      account = mock(IAccount.class);
      when(account.getId()).thenReturn("brett");
      accountRepository = mock(IAccountRepository.class);
      when(accountRepository.find(anyString())).thenReturn(account);
      service = new LoginService(accountRepository);
   }
 
   private void willPasswordMatch(boolean value) {
      when(account.passwordMatches(anyString())).thenReturn(value);
   }
 
   @Test
   public void itShouldSetAccountToLoggedInWhenPasswordMatches() {
      willPasswordMatch(true);
      service.login("brett", "password");
      verify(account, times(1)).setLoggedIn(true);
   }
 
   @Test
   public void itShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
      willPasswordMatch(false);
      for (int i = 0; i < 3; ++i)
         service.login("brett", "password");
      verify(account, times(1)).setRevoked(true);
   }
   
   @Test
   public void itShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
      willPasswordMatch(false);
      service.login("brett", "password");
      verify(account, never()).setLoggedIn(true);
   }
   
   @Test
   public void itShouldNotRevokeSecondAccountAfterTwoFailedAttemptsFirstAccount() {
      willPasswordMatch(false);
 
      IAccount secondAccount = mock(IAccount.class);
      when(secondAccount.passwordMatches(anyString())).thenReturn(false);
      when(accountRepository.find("schuchert")).thenReturn(secondAccount);
 
      service.login("brett", "password");
      service.login("brett", "password");
      service.login("schuchert", "password");
 
      verify(secondAccount, never()).setRevoked(true);
   }
   
   @Test(expected = AccountLoginLimitReachedException.class)
   public void itShouldNotAllowConcurrentLogins() {
      willPasswordMatch(true);
      when(account.isLoggedIn()).thenReturn(true);
      service.login("brett", "password");
   }
   
   @Test(expected = AccountNotFoundException.class)
   public void ItShouldThrowExceptionIfAccountNotFound() {
      when(accountRepository.find("schuchert")).thenReturn(null);
      service.login("schuchert", "password");
   }
   
   @Test(expected = AccountRevokedException.class)
   public void ItShouldNotBePossibleToLogIntoRevokedAccount() {
      willPasswordMatch(true);
      when(account.isRevoked()).thenReturn(true);
      service.login("brett", "password");
   }

   // Cannot login to account with an expired password
   @Test(expected = ExpiredPasswordException.class)
   public void ItShouldNotBePossibleToLogIntoAccountWithExpiredPassword() {
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(false);
	   when(account.isRevoked()).thenReturn(false);
	   when(account.isPasswordExpired()).thenReturn(true);
	   when(account.isPasswordChanged()).thenReturn(false);
	   service.login("brett", "password");
   }
   
   // Can login to account that had an expired password, 
   // after changing the password
   @Test
   public void ItShouldBePossibleToLogIntoAccountWithExpiredPasswordAfterChangingIt() {
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(false);
	   when(account.isRevoked()).thenReturn(false);
	   when(account.isPasswordExpired()).thenReturn(true);
	   when(account.isPasswordChanged()).thenReturn(true);	   
	   when(account.isPasswordAmongLast24()).thenReturn(false);	   
	   service.login("brett", "new_password");
       verify(account, times(1)).setLoggedIn(true);
   }
   
   // Cannot login to account with temporary password
   @Test(expected = TemporaryPasswordException.class)
   public void ItShouldNotBePossibleToLogIntoAccountWithTemporaryPassword() {
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(false);
	   when(account.isRevoked()).thenReturn(false);
	   when(account.isPasswordExpired()).thenReturn(false);
	   when(account.isPasswordTemporary()).thenReturn(true);
	   when(account.isPasswordChanged()).thenReturn(false);
	   service.login("brett", "password");
   }
   
   // Can login to account with temporary password,
   // after changing the password
   @Test
   public void ItShouldBePossibleToLogIntoAccountWithTemporaryPasswordAfterChangingIt() {
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(false);
	   when(account.isRevoked()).thenReturn(false);
	   when(account.isPasswordExpired()).thenReturn(false);
	   when(account.isPasswordTemporary()).thenReturn(true);
	   when(account.isPasswordChanged()).thenReturn(true);
	   when(account.isPasswordAmongLast24()).thenReturn(false);	   
	   service.login("brett", "new_password");
       verify(account, times(1)).setLoggedIn(true);
   }
 
   // Cannot change password to any of previous 24 passwords
   @Test(expected = ReusedPasswordException.class)
   public void ItShouldNotBePossibleToLogIntoAccountWithReusedPassword() {
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(false);
	   when(account.isRevoked()).thenReturn(false);
	   when(account.isPasswordAmongLast24()).thenReturn(true);	
       service.login("brett", "tmp_password");
       verify(account, never()).setLoggedIn(true);
       verify(account, never()).setCanChangePassword(true);
  
   }

   // Can change password to something different than the previous 24 passwords
   @Test
   public void ItShouldBePossibleToLogIntoAccountWithUnreusedPassword() {
	   willPasswordMatch(true);
	   when(account.isLoggedIn()).thenReturn(false);
	   when(account.isRevoked()).thenReturn(false);
	   when(account.isPasswordAmongLast24()).thenReturn(false);
       service.login("brett", "tmp_password");
       verify(account, times(1)).setLoggedIn(true);
       verify(account, times(1)).setCanChangePassword(true);
   
   }
   

}
