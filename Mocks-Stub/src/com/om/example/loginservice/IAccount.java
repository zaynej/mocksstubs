package com.om.example.loginservice;

public interface IAccount {

	boolean passwordMatches(String candidate);
	boolean isLoggedIn();
	boolean isRevoked();
	boolean isPasswordAmongLast24();
	boolean isPasswordChanged();
	boolean isPasswordExpired();
	boolean isPasswordTemporary();
	
	void setLoggedIn(boolean b);
	void setRevoked(boolean b);
	void setPasswordChanged(boolean b);
	void setPasswordExpired(boolean b);
	void setPasswordTemporary(boolean b);

}
